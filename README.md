# Project Infectio

This simulation is created using multithreading, listeners, distance vectors and direction vectors and is written in Java.

![link to demo video](https://gitlab.com/spacecatbug/human-zombie-simulator/-/raw/master/Demo.gif)